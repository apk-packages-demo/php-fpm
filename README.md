# php7-fpm

* [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=php7-fpm)

## TODO
### Check all feature from * are understood and transfered to this project
* https://github.com/travis-util/nginx-sudo

## Main Alpine package demo
* [php7](https://gitlab.com/alpinelinux-packages-demo/php7)

## Alpine packages demo used
* [busybox](https://gitlab.com/alpinelinux-packages-demo/busybox)
* [procps](https://gitlab.com/alpinelinux-packages-demo/procps)

## Documentation used
* PHP Manual > Installation and Configuration > FastCGI Process Manager (FPM) [*Configuration*](http://php.net/manual/en/install.fpm.configuration.php)
* [*nginx showing blank PHP pages*](https://stackoverflow.com/questions/15423500/nginx-showing-blank-php-pages/15424808)
* [*Install NGINX and PHP-FPM running on UNIX file sockets*](https://support.rackspace.com/how-to/install-nginx-and-php-fpm-running-on-unix-file-sockets/)